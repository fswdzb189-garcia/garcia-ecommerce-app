// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';
// import Container from 'react-bootstrap/Container';
// import {useState} from 'react';
import {useContext} from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

// 2 ways to render via function or class
export default function AppNavbar() {

	const { user } = useContext(UserContext);


	// State to store the user information stored in the login page
	//const [user, setUser] = useState(localStorage.getItem("email"));
	/* 
		Syntax:
			localStorage.getItem("propertyName")
		getItem() method that returns the value of a specified object item
	
	 */

	console.log(user);

	return (
		<Navbar bg="light" expand="lg">
	      <Container>
	      	{/* as - serves as anchor tag */}
	        <Navbar.Brand as={ Link } to="/">Best Games Hobby Store</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            {/* <Nav.Link href="#home">Home</Nav.Link> */}
	            <Nav.Link as={ Link } to="/">Home</Nav.Link>
	            {/* <Nav.Link href="#courses">Courses</Nav.Link> */}
	            <Nav.Link as={ Link } to="/courses">Courses</Nav.Link>
	            <Nav.Link as={ Link } to="/products">Products</Nav.Link>
	            {

	            (user.id !== null) ? 
	            	<Nav.Link as={ Link } to="/logout">Logout</Nav.Link>
	            	:

	            <>
	        	{/* parsing error when no parent,  */}
	            <Nav.Link as={ Link } to="/login">Login</Nav.Link>

	            
	            <Nav.Link as={ Link } to="/register">Register</Nav.Link>
	            {/* <Nav.Link as={ Link } to="/register2">Register2</Nav.Link> */}
	            </>

	            }
	           </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>

		)
}