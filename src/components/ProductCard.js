//import { useState, useEffect } from 'react';
// import {Row, Col, Card, Button} from 'react-bootstrap';
import {Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

// export default function CourseCard(props) {
//export default function CourseCard({courseProp}) {
export default function ProductCard({productProp}) {

	




	// Deconstruct the course properties into their own variables
	const { name, description, price, _id } = productProp;
	console.log(productProp);



	return (

			
				<Card>
				      <Card.Body>
				      	<Card.Title>{name}</Card.Title>
				      
				        <Card.Text>
				          Description:<br/> {description}
				         <br/><br/>

				          Price:<br/>
				          PhP {price}
				        </Card.Text>
				        <Button variant="primary" as={Link} to={`/api/products/${_id}`}>Details</Button>
				      
				      </Card.Body>
				</Card>
	


		)



}