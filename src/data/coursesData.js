const coursesData = [
	
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis omnis qui sunt delectus, a officiis amet doloremque explicabo in mollitia, odio nemo ratione atque nam quasi laudantium architecto aut earum.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis omnis qui sunt delectus, a officiis amet doloremque explicabo in mollitia, odio nemo ratione atque nam quasi laudantium architecto aut earum.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis omnis qui sunt delectus, a officiis amet doloremque explicabo in mollitia, odio nemo ratione atque nam quasi laudantium architecto aut earum.",
		price: 55000,
		onOffer: true
	}
]

export default coursesData; 

