import {Fragment, useEffect, useState} from 'react';
import CourseCard from '../components/CourseCard';
import ProductCard from '../components/ProductCard';




export default function Products() {

	

	const [products, setProducts] = useState([])

	useEffect(() => {

		fetch('http://localhost:4008/api/products/allactiveproducts')
		.then(res => res.json())
		.then( data => {

			console.log(data)

			setProducts(data.map(product => {

				return (

					<ProductCard key={product._id} productProp={product} />

					)

			}))

		})

	}, [])




	return(

		//you can use <> instead of <Fragments>
		//<>
		<Fragment> 
			<h1>Products</h1>
			{products}
		</Fragment>
		//</>

		)
}